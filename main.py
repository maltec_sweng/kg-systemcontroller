import json
import time
from system_controller import SystemController

# For opening Json content
with open("config.json") as f:
    config = json.load(f)

ctrl = SystemController(config)
ctrl.connect_mqtt()

while True:
    time.sleep(1)