from __future__ import annotations
import base64
import datetime
import json
import requests
import threading
from paho.mqtt.client import Client as MqttClient, MQTTMessage

class SystemController:
    
    state: str 
    timer: threading.Timer
    current_activity_id: int
    stove_state: bool

    def __init__(self, conf : dict) -> None:
        # Initialize attributes
        self.state = 'INACTIVE'
        self.current_activity_id = None
        self.stove_state = False
        self.timer = threading.Timer(0, None)
        
        # Parse configuration file
        self.timer_duration = conf['timer_duration']['value']
        unit = conf['timer_duration']['unit']
        if unit.lower() in ['m', 'min', 'minute', 'minutes']:
            self.timer_duration *= 60
        elif unit.lower() in ['h', 'hour', 'hours']:
            self.timer_duration *= 3600
        self.timer_duration = int(self.timer_duration)
        
        self.mqtt_conf = conf['mqtt']
        self.ws_conf = conf['webservice']
        self.rooms = conf['rooms']

        # Get mqtt information
        self.mqtt_host = self.mqtt_conf['host']
        self.mqtt_port = 1883
        if 'port' in self.mqtt_conf:
            self.mqtt_port = self.mqtt_conf['port']
        
        if 'username' in self.mqtt_conf and 'password' in self.mqtt_conf:
            self.mqtt_username = self.mqtt_conf['username']
            self.mqtt_password = self.mqtt_conf['password']
        else:
            self.mqtt_anonymous = True

        # Create device dict indexed by topic and add all topics to list
        self.devices = {} 
        self.topics = []

        for room_name, room in self.rooms.items():
            for device in room['devices']:
                d = {
                    "room" : room_name,
                    "type" : device['type']
                }
                self.topics.append(device['topic'])
                self.devices[device['topic']] = d
            room['occupancy'] = False

        self.mqtt = MqttClient()

        # Event callbacks
        self.mqtt.on_connect = self.on_connect
        self.mqtt.on_message = self.on_message
        
        self.test_ws_connection()

    def connect_mqtt(self) -> None:
        """
        Establishes a connection to the MQTT server if no connection has been established
        """
        if self.mqtt.is_connected():   # if a connection is already established
            return
        else:
            # Check if username and password are required to connect
            if not self.mqtt_anonymous:
                self.mqtt.username_pw_set(self.mqtt_username, self.mqtt_password)
            
            self.mqtt.connect(self.mqtt_host, self.mqtt_port)

            # Subscribe to all topics
            for topic in self.topics:
                self.mqtt.subscribe(topic)

            self.mqtt.loop_start()   # start mqtt client in a new thread

    def disconnect_mqtt(self) -> None:
        """
        Disconnects from the MQTT server
        """
        self.mqtt.loop_stop()
        self.mqtt.disconnect()

    def on_connect(self, client, userdata, flags, rc) -> None:   # rc = return code
        """
        Callback when attempting to connect
        """
        if rc == 0:
            print("MQTT Connected OK. Returned code ", rc)
        else:
            print("MQTT Bad connection. Returned code ", rc)

    def on_message(self, client, userdata, msg: MQTTMessage) -> None:
        """
        Callback when receiving a message
        """
        # Update current parsed msg
        try:
            payload = json.loads(msg.payload.decode('utf-8'))
        except json.JSONDecodeError:
            payload = {}

        if msg.topic in self.devices:
            current_device = self.devices[msg.topic]
            room = current_device['room']
            device_type = current_device['type']

            print(f"\n{room} {device_type} {msg.topic} {payload}")
            if device_type == 'pir':
                self.on_pir_message(msg.topic, payload['occupancy'])
            elif device_type == 'plug' and room == 'kitchen':
                self.on_plug_message(msg.topic, payload['state'])

            print(f"State: {self.state}")

    def on_pir_message(self, topic: str, occupancy: bool) -> None:
        """
        Checks whether the user has left the kitchen or has come back to the kitchen
        """
        device = self.devices[topic]
        room = device['room']
        if room == 'kitchen':
            # Check if the occupancy actually changed
            if self.rooms[room]['occupancy'] != occupancy:
                if occupancy:
                    # Someone entered the kitchen
                    self.user_back()
                elif not occupancy and self.stove_state == 'ON':
                    # Someone left the kitchen
                    self.user_left()
        self.rooms[room]['occupancy'] = occupancy
    
    def on_plug_message(self, topic: str, state: str) -> None:
        """
        Activates/deactivates the system based on the stove state and system state if the room is kitchen.
        - Deactivates the system if the stove is off, but the state is active 
        - Activates the system if the stove is on, but the state is inactive
        """
        device = self.devices[topic]
        room = device['room']
        if room == 'kitchen':
            if self.state == 'ACTIVE' and state == 'OFF':
                self.deactivate_system()
            elif self.state == 'INACTIVE' and state == 'ON':
                self.activate_system()
            self.stove_state = state
        self.devices[topic]['state'] = state


    def activate_system(self) -> None:
        """
        Switches the system to active state, and stores the start time of the activity 
        and the time of the event 'activate' to the API
        """
        self.state = 'ACTIVE'
        
        now = self.get_current_time()
        self.store_activity_start_time(now)
        self.store_event('activate', now)

    def deactivate_system(self) -> None:
        """
        Switches the system to inactive state, and stores the time of the event 'deactivate'
        and the end time of the activity to the API
        """
        self.state = 'INACTIVE'
        
        now = self.get_current_time()
        self.store_event('deactivate', now)
        self.store_activity_end_time(now)

    def stop_alarm(self) -> None:
        """
        Stops the alarm if the system is in alarmed state, stores the time of 'alarm_stop' event to the API,
        and calls method to deactivate system and method to turn off lights
        """
        if self.state == 'ALARMED':
            print("Alarm stopped")
            end_time = self.get_current_time()
            self.store_event('alarm_stop', end_time)
            
            self.deactivate_system()
            self.turn_off_lights()


    def user_left(self) -> None:
        """
        Stores the time of the event 'leave' to the API and starts a timer (thread).
        """
        if self.state != 'INACTIVE':
            print("User left")
            now = self.get_current_time()
            self.store_event('leave', now)

            # Start timer
            self.timer = threading.Timer(self.timer_duration, self.start_alarm)
            self.timer.start()

    def user_back(self) -> None:
        """
        Stores the time of the event 'enter' to the API and stops the timer
        """
        if self.state != 'INACTIVE':
            print("User back")
            self.timer.cancel()
            now = self.get_current_time()

            self.store_event('enter', now)
            self.stop_alarm()

    def occupied_rooms(self) -> list[str]:
        """
        Get a list of all occupied rooms

        Returns:
            list[str]: a list of names of all occupied rooms
        """
        rooms = []
        for room, content in self.rooms.items():
            if 'occupancy' in content and content['occupancy']:
                rooms.append(room)
        return rooms
    
    def start_alarm(self) -> None:
        """
        Switches the system to alarmed state, stores the time of the event 'alarm_start',
        turns off the stove in the kitchen and calls the method to turn on lights
        """
        # Store event 'alarm_start'
        self.state = 'ALARMED'
        print("Alarm started")
        
        self.store_event('alarm_start', self.get_current_time())
        
        # Turn off stove
        for device in self.rooms['kitchen']['devices']:
            if device['type'] == 'plug':
                kplug_topic = device['topic']
        self.mqtt.publish(kplug_topic+"/set", '{"state": "OFF"}')
        self.stove_state = 'OFF'
        self.turn_on_lights()
    
    def turn_on_lights(self) -> None:
        """ 
        Finds all occupied rooms and turn on the lights.
        If no room is occupied, the lights in all rooms are turned on
        """
        topics = []
        occupied_rooms = self.occupied_rooms()
        if occupied_rooms:
            print(f"The following rooms are occupied: {occupied_rooms}")
            for room in occupied_rooms:
                for device in self.rooms[room]['devices']:
                    if device['type'] == 'led':
                        topics.append(device['topic'])
        else:
            print('User is away')
            for topic, device in self.devices.items():
                if device['type'] == 'led':
                    topics.append(topic)

        print(f"Turning on lights in the following topics: {topics}")
        # Raise alarm in user's room if user is in another room
        for topic in topics:
            t = topic + "/set"
            self.mqtt.publish(t, '{"state": "ON"}') 
    
    def turn_off_lights(self) -> None:
        """
        Turns off the lights
        """
        topics = []
        for topic, device in self.devices.items():
            if device['type'] == 'led':
                topics.append(topic)
        print(f"Turning off lights in the following topics: {topics}")

        for topic in topics:
            t = topic + "/set"
            self.mqtt.publish(t, '{"state": "OFF"}') 

    def get_state(self) -> str:
        """ 
        Returns the state that the system is currently in 
        """
        return self.state

    def get_current_time(self) -> datetime.datetime:
        """ 
        Returns the current time 
        """
        return datetime.datetime.utcnow()


    def send_api_request(self, url, data=None):
        """
        Creates connection to the webservice, and either gets or posts requests.
        """
        aid = self.ws_conf['access_id']
        pwd = self.ws_conf['password']

        credentials_plain = f'{aid}:{pwd}'
        cred_encoded = base64.b64encode(bytes(credentials_plain, 'utf8')).decode("utf8")

        headers = {'Authorization': f'Basic {cred_encoded}'}

        # Compose the url
        full_url = ''
        full_url += self.ws_conf['proto'] if 'proto' in self.ws_conf else 'http'
        full_url += '://'
        full_url += self.ws_conf['host']
        if 'port' in self.ws_conf:
            full_url += ':' + self.ws_conf['port']
        full_url += '/api/ms'
        full_url += url
        
        if data is None:
            # GET request
            res = requests.get(full_url, headers=headers)
            return res.json()
        else:
            # POST requests
            print(f"Sending POST request to {full_url} with data {data}")
            
            headers['Content-Type'] = 'application/json'
            json_data = json.dumps(data)
            res = requests.post(full_url, json_data, headers=headers)
            return res.json()

    def test_ws_connection(self) -> bool:
        """
        Checks connection to the webservice
        """
        res = self.send_api_request('/')
        succ = 'message' in res and res['message'] == 'success'
        if succ:
            print("Connection to webservice successful")
        else:
            print("Connection to webservice failed")
        return succ

    def store_activity_start_time(self, start_time: datetime.datetime) -> None:
        """
        Posts the start time of an activity to the API and saves the current id of the activity
        """
        res = self.send_api_request(
            '/activity/start', 
            {
                'start_time': start_time.strftime('%Y-%m-%d %H:%M:%S')
            })
        self.current_activity_id = res['id']
        print(f"Activity started with id {self.current_activity_id}")

    def store_activity_end_time(self, end_time: datetime.datetime) -> None:
        """
        Posts the end time of an activity to the API
        """
        res = self.send_api_request(
            '/activity/stop', 
            {
                'id':self.current_activity_id,
                'end_time': end_time.strftime('%Y-%m-%d %H:%M:%S')
            })
        assert self.current_activity_id == res['id']
        self.current_activity_id = None

    def store_event(self, event_type : str, time: datetime.datetime) -> None:
        """
        Posts an event that happens during an activity to the API
        """
        res = self.send_api_request(
            '/event/add', 
            {
                'activity_id': self.current_activity_id,
                'event_type': event_type,
                'time': time.strftime('%Y-%m-%d %H:%M:%S')
            })
        event_id = res['id']
        print(f"Event stored with id {event_id}")